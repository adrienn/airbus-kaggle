# -*- coding: utf-8 -*-

import keras.optimizers as opts


def Adam(params):
    return opts.Adam(**params)


def RMSprop(params):
    return opts.RMSprop(**params)


def SGD(params):
    return opts.SGD(**params)


def get_optimizer(name, params):
    if name not in ['Adam', 'RMSprop', 'SGD']:
        raise ValueError('optimizer not implemented')

    if name == 'Adam':
        return Adam(params)

    elif name == 'RMSprop':
        return RMSprop(params)

    elif name == 'SGD':
        return SGD(params)

# -*- coding: utf-8 -*-

from keras.losses import binary_crossentropy
import numpy as np
from keras import backend as K


def dice_loss(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = y_true_f * y_pred_f
    score = (2. * K.sum(intersection) + smooth) / (K.sum(y_true_f) +
                                                   K.sum(y_pred_f) + smooth)
    return 1. - score


def bce_dice_loss(y_true, y_pred):
    return (binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred))


def IoU(y_true, y_pred, threshold):
    smooth = 1.
    y_true = K.cast(K.greater(K.clip(y_true, 0, 1), threshold), K.floatx())
    y_pred = K.cast(K.greater(K.clip(y_pred, 0, 1), threshold), K.floatx())
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = y_true_f * y_pred_f
    score = (2. * K.sum(intersection) + smooth) / (K.sum(y_true_f) +
                                                   K.sum(y_pred_f) + smooth)
    return score


def IoU_neg(y_true, y_pred, threshold=.5):
    smooth = 1.
    y_true = K.cast(K.less(K.clip(y_true, 0, 1), threshold), K.floatx())
    y_pred = K.cast(K.less(K.clip(y_pred, 0, 1), threshold), K.floatx())
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = y_true_f * y_pred_f
    score = (2. * K.sum(intersection) + smooth) / (K.sum(y_true_f) +
                                                   K.sum(y_pred_f) + smooth)
    return score


def score_iou(y_true, y_pred, threshold=.5):
    return 1 - IoU(y_true, y_pred, threshold=.5)


def score_iou_neg(y_true, y_pred, threshold=.5):
    return 1 - IoU_neg(y_true, y_pred, threshold=.5)


def fscore(tp, fn, fp, beta=2.):
    if tp + fn + fp < 1:
        return 1.
    num = (1 + beta ** 2) * tp
    return num / (num + beta ** 2 * fn + fp)


def confusion_counts(predict_mask_seq, truth_mask_seq, iou_thresh=0.5):
    if len(predict_mask_seq) + len(truth_mask_seq) == 0:
        tp, fn, fp = 0, 0, 0
        return tp, fn, fp

    pred_hits = np.zeros(len(predict_mask_seq), dtype=np.bool)  # 0 miss, 1 hit
    truth_hits = np.zeros(len(truth_mask_seq), dtype=np.bool)  # 0 miss, 1 hit

    for p, pred_mask in enumerate(predict_mask_seq):
        for t, truth_mask in enumerate(predict_mask_seq):
            if IoU(pred_mask, truth_mask) > iou_thresh:
                truth_hits[t] = True
                pred_hits[p] = True

    tp = np.sum(pred_hits)
    fn = len(truth_mask_seq) - np.sum(truth_hits)
    fp = len(predict_mask_seq) - tp

    return tp, fn, fp


def mean_fscore(predict_mask_seq, truth_mask_seq,
                iou_thresholds=[0.5, 0.55, 0.6, 0.65, 0.7,
                                0.75, 0.8, 0.85, 0.9, 0.95], beta=2.):
    """ calculates the average FScore for the predictions in an image over
    the iou_thresholds sets.
    predict_mask_seq: list of masks of the predicted objects in the image
    truth_mask_seq: list of masks of ground-truth objects in the image
    """
    return np.mean(
        [fscore(tp, fn, fp, beta) for (tp, fn, fp) in
            [confusion_counts(predict_mask_seq, truth_mask_seq, iou_thresh)
                for iou_thresh in iou_thresholds]])


def get_metrics(name):
    if name not in ['bce_dice_loss', 'dice_loss']:
        raise ValueError('metric not implemented')

    if name == 'bce_dice_loss':
        return bce_dice_loss
    elif name == 'dice_loss':
        return dice_loss

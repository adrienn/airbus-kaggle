# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from skimage.data import imread
from .params import (PATH_TRAIN_IMG,
                     IMG_SHAPE, PATH_TRAIN_CSV)
from skimage.morphology import label


def read_data(path_csv=PATH_TRAIN_CSV):
    """ Read csv train csv and return and dataframe
    """
    df = pd.read_csv(path_csv)
    df = (df.groupby('ImageId')
          .apply(group_img)
          .reset_index(name='EncodedPixels'))
    df['img_path'] = PATH_TRAIN_IMG + df.ImageId
    return df


def rle_encode(img):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    source: https://www.kaggle.com/paulorzp/run-length-encode-and-decode
    '''
    pixels = img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)


def rle_decode(mask_rle, shape=IMG_SHAPE[:-1]):
    '''
    mask_rle: run-length as string formated (start length)
    shape: (height,width) of array to return
    Returns numpy array, 1 - mask, 0 - background

    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int)
                       for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape).T


def multi_rle_encode(img, threshold=.5):
    labels = label(img > threshold)
    if img.ndim > 2:
        return [rle_encode(np.sum(labels == k, axis=2))
                for k in np.unique(labels[labels > 0])]
    else:
        return [rle_encode(labels == k)
                for k in np.unique(labels[labels > 0])]


def masks_as_image(mask_list, all_masks=None):
    all_masks = np.zeros(IMG_SHAPE[:-1])
    for mask in mask_list:
        all_masks += rle_decode(mask)
    return all_masks


def group_img(x):
    enc = x.EncodedPixels
    if any(pd.isnull(enc)):
        return np.NaN
    return x.EncodedPixels.tolist()


def read_img_and_mask(x):
    """ return img and mask
    params:
    x: dict with EncodedPixels and img_path as keys
    """
    mask_list = x['EncodedPixels']
    if type(mask_list) is float:
        mask = np.zeros(IMG_SHAPE[:-1])
    else:
        mask = masks_as_image(mask_list=mask_list)
    img = imread(x['img_path'])
    return img, mask


def generator(df_train, batch_size=32):
    """
    Generator for tensorflow/keras training
    """
    while True:
        df_sample = df_train.sample(batch_size)
        imgs, masks = [], []
        for j in range(batch_size):
            x = df_sample.iloc[j]
            try:
                img, mask = read_img_and_mask(x)
                imgs.append(img/255.)
                masks.append(mask)
            except:
                continue
        yield np.stack(imgs), np.expand_dims(np.stack(masks), 3)

# -*- coding: utf-8 -*-

from src.utils import (read_data,
                       generator)
from keras.callbacks import (ModelCheckpoint,
                             EarlyStopping,
                             ReduceLROnPlateau)
from sklearn.model_selection import train_test_split
import imp
import yaml
import os
import pathlib
from keras.callbacks import TensorBoard
from time import time
import tensorflow as tf


def is_valid_file(parser, arg):
    """
    Check if arg is a valid file that already exists on the file system.
    Parameters
    ----------
    parser : argparse object
    arg : str
    Returns
    -------
    arg
    """
    arg = os.path.abspath(arg)
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg


def get_parser():
    """Get parser object for script xy.py."""
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--file",
                        dest="filename",
                        type=lambda x: is_valid_file(parser, x),
                        help="experiment definition file",
                        metavar="FILE.yaml",
                        required=True)
    parser.add_argument("-g", "--gpu",
                        dest="gpu",
                        help="gpu device, '0', '0,1,2'",
                        default='0',
                        required=False)
    return parser


def create_path(path):
    p = pathlib.Path(path)
    parent = p.parents[0]
    if not parent.exists():
        parent.mkdir()


if __name__ == '__main__':
    args = get_parser().parse_args()
    with open(args.filename, 'r') as stream:
        cfg = yaml.load(stream)

    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    batch_size = cfg['training']['batch_size']
    steps_per_epoch = cfg['training']['steps_per_epoch']
    parameters = cfg['model']['parameters']
    model_ = imp.load_source('create_model', cfg['model']['path_file'])
    weight_path = 'weights/{}/{}'.format(
        pathlib.Path(args.filename).stem,
        cfg['training']['path_weights'])
    create_path(weight_path)
    print('model loaded')

    df = read_data()
    df['n_ship'] = df.EncodedPixels.apply(
        lambda x: 0 if type(x) is float else len(x))
    df_train, df_test = train_test_split(df, test_size=.3,
                                         stratify=df['n_ship'])
    my_gen = generator(df_train, batch_size=batch_size)
    my_gen_test = generator(df_test, batch_size=batch_size)

    loss = imp.load_source(cfg['loss']['name'], cfg['loss']['path_file'])
    loss_fnc = loss.get_metrics(cfg['loss']['name'])

    optimizer = imp.load_source(cfg['optimizer']['name'],
                                cfg['optimizer']['path_file'])
    optimizer_fnc = optimizer.get_optimizer(cfg['optimizer']['name'],
                                            cfg['optimizer']['parameters'])

    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

    checkpoint = ModelCheckpoint(weight_path, monitor='val_loss',
                                 verbose=1,
                                 mode='min', save_weights_only=True)
    reduceLROnPlat = ReduceLROnPlateau(monitor='val_loss',
                                       factor=0.2,
                                       patience=6,
                                       verbose=1, mode='min',
                                       epsilon=0.0001, cooldown=2,
                                       min_lr=1e-6)
    # probably needs to be more patient
    early = EarlyStopping(monitor='val_loss',
                          mode="min",
                          patience=20)
    callbacks_list = [checkpoint, early, reduceLROnPlat, tensorboard]

    model = model_.create_model(**parameters)
    model.compile(optimizer=optimizer_fnc,
                  loss=loss_fnc,
                  metrics=['binary_crossentropy',
                           'binary_accuracy',
                           loss_fnc])
    loss_history = [model.fit_generator(
        my_gen,
        steps_per_epoch=steps_per_epoch,
        epochs=1000,
        validation_data=my_gen_test,
        validation_steps=int(300*32/batch_size),
        callbacks=callbacks_list,
        workers=1)]

    # model.fit_generator(my_gen, steps_per_epoch=300,
    #                     epochs=1, callbacks=[model_checkpoint])

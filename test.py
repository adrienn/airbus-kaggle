# -*- coding: utf-8 -*-

from models.unet import create_model
from src.utils import multi_rle_encode
from glob import glob
from skimage.data import imread
import numpy as np
import imp
import pandas as pd
import yaml


PATH_TEST_IMG = './test/'


def generator_test(batch_size=32):
    path_imgs = glob('{}/*.jpg'.format(PATH_TEST_IMG))
    imgs = []
    paths = []
    for path in path_imgs:
        img = imread(path)
        imgs.append(img/255.)
        paths.append(path)
        if len(imgs) >= batch_size:
            yield np.stack(imgs), paths
            imgs = []
            paths = []
    if len(paths):
        yield np.stack(imgs), paths


def get_parser():
    """Get parser object for script xy.py."""
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--file",
                        dest="filename",
                        help="experiment definition file",
                        metavar="FILE.yaml",
                        required=True)
    parser.add_argument("-w", "--weights",
                        dest="weights",
                        help="weights file path",
                        required=True)
    parser.add_argument("-o", "--output",
                        dest="output",
                        default=None,
                        required=False)
    return parser


if __name__ == '__main__':
    total_imgs = len(glob('{}/*.jpg'.format(PATH_TEST_IMG)))
    args = get_parser().parse_args()

    if args.output:
        output = args.output
    else:
        output = args.output.split('/')[-1]
        output = 'data/{}'.format(
            output.replace('yaml', 'csv'))
    with open(args.filename, 'r') as stream:
        cfg = yaml.load(stream)

    batch_size = cfg['training']['batch_size']
    parameters = cfg['model']['parameters']
    model_ = imp.load_source('create_model', cfg['model']['path_file'])
    model = model_.create_model(**parameters)

    model.load_weights(args.weights)
    generator = generator_test(batch_size)
    results = []
    n_imgs = 0
    for x, y in generator:
        predictions = model.predict(x)
        for i in range(predictions.shape[0]):
            rle = multi_rle_encode(predictions[i, :, :, 0])
            if len(rle) == 0:
                res = [{'ImageId': y[i],
                       'EncodedPixels': ''}]
            else:
                res = []
                for r in rle:
                    res.append(
                        {'ImageId': y[i],
                         'EncodedPixels': r})

            results += res
        n_imgs += batch_size
        print('{}/{}'.format(n_imgs,
                             total_imgs))
    df = pd.DataFrame(results)
    df.to_csv(output)


def make_submissions():
    from datetime import datetime
    df = pd.read_csv('data/submission_2018-08-14.csv')

    now = datetime.now()
    date = now.strftime('%Y-%m-%dT%H-%M')
    df.ImageId = df['ImageId'].str.replace('./test/', '')
    df[['ImageId', 'EncodedPixels']].to_csv('submissions/{}.csv'.format(date),
                                            index=False)

    df = pd.DataFrame.from_csv('data/submission_2018-08-14.csv')
    df = pd.DataFrame.from_csv('data/tt.csv')
    df.dropna(inplace=True)
    len_encoded = df.EncodedPixels.apply(lambda x: len(x.split(' ')))
    df = df[(len_encoded > 0) & (len_encoded < 600)]
    x_set = set(df.ImageId.tolist())
    y_set = set(glob('./test/*.jpg'))
    n = y_set-x_set
    df_ap = pd.DataFrame({'EncodedPixels': '', 'ImageId': list(n)})
    df = df.append(df_ap, ignore_index=True)
    now = datetime.now()
    date = now.strftime('%Y-%m-%dT%H-%M')
    df.ImageId = df['ImageId'].str.replace('./test/', '')
    df[['ImageId', 'EncodedPixels']].to_csv('submissions/{}.csv'.format(date),
                                            index=False)

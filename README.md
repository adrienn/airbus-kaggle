# Kaggle


## Installation

Après clonage du git

```bash
kaggle competitions download -c airbus-ship-detection
mkdir train
unzip train.zip -d train/
unzip train_ship_segmentations.csv.zip
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements
```

```bash
apt -y install git vim
git clone https://gitlab.com/adrienn/airbus-kaggle.git
cd airbus-kaggle
./run.sh experiments/training003.yaml
```

```bash
scp -r -P 7113 root@52.204.230.7:/root/airbus-kaggle/weights/ ./
```

then roule ma poule !

## Modèles

* Unet: https://arxiv.org/abs/1505.04597


## TODO

* Data Augmentation
* Trouver une bonne loss
* sampling stratifié en fonction du nombre de bateaux

* tester:
** https://github.com/ternaus/TernausNet
** https://github.com/qqgeogor/keras-segmentation-networks/blob/master/deeplabv3.py
** https://github.com/selimsef/dsb2018_topcoders
**
https://github.com/petrosgk/Kaggle-Carvana-Image-Masking-Challenge/blob/master/model/losses.py
 pour les losses weighted_bce
* https://github.com/selimsef/dsb2018_topcoders/tree/master/victor 
 ** https://www.kaggle.com/lyakaap/weighing-boundary-pixels-loss-script-by-keras2
 * https://github.com/lyakaap/Kaggle-Carvana-3rd-Place-Solution/blob/master/model.pyhttps://github.com/lyakaap/Kaggle-Carvana-3rd-Place-Solution/blob/master/model.py

## Papiers:

* Review: https://arxiv.org/pdf/1704.06857.pdf
* TernausNet: https://arxiv.org/pdf/1801.05746.pdf
* https://arxiv.org/pdf/1706.06169.pdf


#!/bin/bash

mkdir ~/.kaggle
echo '{"username":"foryoucousteau","key":"e7627b5b1f84d134dcd2b5c88522cb4f"}' > ~/.kaggle/kaggle.json
pip install kaggle virtualenv
kaggle competitions download -f train_ship_segmentations.csv -c airbus-ship-detection
kaggle competitions download -f train.zip -c airbus-ship-detection
mkdir train
unzip train.zip -d train/
unzip train_ship_segmentations.csv.zip
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt
python train.py -f $1

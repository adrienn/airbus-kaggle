# -*- coding: utf-8 -*-

from keras.models import Input, Model
from keras.layers import (Conv2D,
                          Conv2DTranspose,
                          MaxPooling2D,
                          BatchNormalization,
                          Dropout,
                          Activation,
                          UpSampling2D,
                          concatenate)


def conv_block(inputs, dim, activation='relu',
               bn=False, do=False, mp=True, dilation_rate=1, order=1):
    if order == 1:
        conv = Conv2D(dim, (3, 3), padding='same')(inputs)
        conv = BatchNormalization()(conv) if bn else conv
        conv = Activation(activation)(conv)
    elif order == 2:
        conv = Conv2D(dim, (3, 3), padding='same')(inputs)
        conv = Activation(activation)(conv)
        conv = BatchNormalization()(conv) if bn else conv
    elif order == 3:
        conv = BatchNormalization()(conv) if bn else conv
        conv = Conv2D(dim, (3, 3), padding='same')(inputs)
        conv = Activation(activation)(conv)

    conv = Dropout(do)(conv) if do else conv
    if order == 1:
        conv = Conv2D(dim, (3, 3), padding='same',
                      dilation_rate=dilation_rate)(conv)
        conv = BatchNormalization()(conv) if bn else conv
        conv = Activation(activation)(conv)
    elif order == 2:
        conv = Conv2D(dim, (3, 3), padding='same',
                      dilation_rate=dilation_rate)(conv)
        conv = Activation(activation)(conv)
        conv = BatchNormalization()(conv) if bn else conv
    elif order == 3:
        conv = BatchNormalization()(conv) if bn else conv
        conv = Conv2D(dim, (3, 3), padding='same',
                      dilation_rate=dilation_rate)(conv)
        conv = Activation(activation)(conv)

    conv = Dropout(do)(conv) if do else conv
    pool = (MaxPooling2D(pool_size=(2, 2))(conv)
            if mp else Conv2D(dim, 3,
                              strides=2, padding='same')(conv))
    return conv, pool


def deconv_block(inputs, conv_feats, dim, activation='relu',
                 bn=False, do=False, order=1, upsampling=False):
    if upsampling:
        up = concatenate(
            [Conv2D(dim, 2, activation=activation,
                    padding='same')(UpSampling2D(size=(2, 2)))(inputs),
             conv_feats], axis=3)
    else:
        up = concatenate(
            [Conv2DTranspose(dim, (2, 2), strides=(2, 2),
                             padding='same')(inputs), conv_feats], axis=3)

    if order == 1:
        conv = Conv2D(dim, (3, 3), padding='same')(up)
        conv = BatchNormalization()(conv) if bn else conv
        conv = Activation(activation)(conv)
    else:
        conv = Conv2D(dim, (3, 3), padding='same')(up)
        conv = Activation(activation)(conv)
        conv = BatchNormalization()(conv) if bn else conv

    conv = Dropout(do)(conv) if do else conv
    if order == 1:
        conv = Conv2D(dim, (3, 3), padding='same')(conv)
        conv = BatchNormalization()(conv) if bn else conv
        conv = Activation(activation)(conv)
    elif order == 2:
        conv = Conv2D(dim, (3, 3), padding='same')(conv)
        conv = Activation(activation)(conv)
        conv = BatchNormalization()(conv) if bn else conv
    elif order == 3:
        conv = BatchNormalization()(conv) if bn else conv
        conv = Conv2D(dim, (3, 3), padding='same')(conv)
        conv = Activation(activation)(conv)
    conv = Dropout(do)(conv) if do else conv
    return up, conv


def create_model(img_shape=(768, 768, 3), depth=4,
                 start_ch=32,
                 activation='relu',
                 dropout=False,
                 batch_norm=False,
                 max_pool=True,
                 dilation_rate=1,
                 order=1, upsampling=False):
    """
    order: 1: conv -> batch_norm -> activation -> dropout -> conv
    order: 2: conv -> activation -> batch_norm -> dropout -> conv
    order: 3: batch_norm -> conv -> activation -> dropout -> batch_norm
    """
    inputs_img = Input(img_shape)
    inputs = inputs_img

    conv_layers = []
    for i in range(depth):
        conv, inputs = conv_block(inputs, start_ch*2*(i+1), activation,
                                  batch_norm, dropout, max_pool,
                                  dilation_rate=dilation_rate, order=order)
        conv_layers.append(conv)

    if order == 1:
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
        conv_mid = Activation(activation)(conv_mid)
    elif order == 2:
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = Activation(activation)(conv_mid)
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
    elif order == 3:
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = Activation(activation)(conv_mid)
    conv_mid = Dropout(dropout)(conv_mid) if dropout else conv_mid

    if order == 1:
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
        conv_mid = Activation(activation)(conv_mid)
    elif order == 2:
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = Activation(activation)(conv_mid)
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
    elif order == 3:
        conv_mid = BatchNormalization()(conv_mid) if batch_norm else conv_mid
        conv_mid = Conv2D(start_ch*2*(depth+1), (3, 3), padding='same')(inputs)
        conv_mid = Activation(activation)(conv_mid)
    conv_mid = Dropout(dropout)(conv_mid) if dropout else conv_mid

    inputs = conv_mid
    for i in reversed(range(depth)):
        conv_feats = conv_layers.pop()
        up, inputs = deconv_block(inputs, conv_feats,
                                  start_ch*2*(i+1), activation,
                                  batch_norm, dropout, order=order,
                                  upsampling=upsampling)

    conv_last = Conv2D(1, (1, 1), activation='sigmoid')(inputs)

    model = Model(inputs=inputs_img, outputs=conv_last)

    return model

# -*- coding: utf-8 -*-

from keras.layers import (Conv2D,
                          BatchNormalization,
                          Activation,
                          AveragePooling2D,
                          Input,
                          Lambda,
                          Dropout)

from keras.layers.merge import Concatenate, Add

from keras.models import Model
import tensorflow as tf


def block_conv2d(inputs, filters, stage=0, block=0, kernel_size=3,
                 stride=None, activation='relu'):
    """
    Two dimensional block for resnet
    """

    if block != 0 or stage == 0:
        stride = 1
    else:
        stride = 2
        stride = 1
    y = Conv2D(filters, kernel_size, strides=stride,
               padding='same')(inputs)
    y = BatchNormalization()(y)
    y = Activation(activation)(y)

    y = Conv2D(filters, kernel_size, strides=stride,
               padding='same')(y)
    y = BatchNormalization()(y)

    if block == 0:
        shortcut = Conv2D(filters, (1, 1), strides=stride)(y)
        shortcut = BatchNormalization()(shortcut)
    else:
        shortcut = inputs

    y = Add()([y, shortcut])
    y = Activation(activation)(y)
    return y


def resnet(inputs, img_shape=(768, 768, 3),
           start_ch=8, activation='relu',
           blocks=[2, 2, 2, 2]):
    """
    Resnet as features extractor
    """
    n_filters = start_ch

    x = inputs
    blocks_output = []
    for stage_id, n_blocks in enumerate(blocks):
        for block_id in range(n_blocks):
            x = block_conv2d(x, n_filters, stage=stage_id, block=block_id)

        n_filters *= 2
        blocks_output.append(x)

    return x, blocks_output


def create_model(img_shape=(768, 768, 3),
                 start_ch=8,
                 blocks_resnet=[2, 2, 2, 2],
                 kernel_strides_pyramid=[90, 45, 30, 15],
                 n_filters_pyramids=8,
                 n_filters_output=128,
                 dropout=.1,
                 output_ch=1,
                 downsampling=False,
                 activation='relu'):
    """
    Pyramid Scene Parsing Network: https://arxiv.org/abs/1612.01105

    Parameters:
    ==========
    img_shape: tuple, shape of input image
    start_ch: int, number of filters for the first layer of the resnet
    blocks_resnet: list, list of int with the number of conv2d block for resnet
    kernel_strides_pyramid: list, list of int with the kernel size for each pyramids (the higher, the higher is the downsampling)
    n_filters_pyramids: int, number of filters for each pyramids
    n_filters_output: int, number of filters for the last Conv2D layer after concatenation of each output of the pyramids and the resnet
    dropout: float, proportion of dropout at the end of the network
    output_ch: int, number of output of the network
    """
    inputs_img = Input(img_shape)

    if downsampling:
        img_res = AveragePooling2D(
            pool_size=downsampling,
            strides=downsampling, padding='same')(inputs_img)
        output_shape = [int(x/downsampling) for x in img_shape[0:2]]
    else:
        output_shape = img_shape[0:2]
        img_res = inputs_img

    output_resnet, blocks_resnet_ = resnet(img_res,
                                           img_shape=img_shape,
                                           start_ch=start_ch,
                                           activation=activation,
                                           blocks=blocks_resnet)

    res = [output_resnet]
    for i, kernel_size in enumerate(kernel_strides_pyramid):
        res.append(
            pyramide(output_resnet,
                     kernel_size=kernel_size,
                     stride=kernel_size,
                     n_filters=n_filters_pyramids,
                     output_shape=output_shape,
                     activation=activation))

    out = Concatenate(axis=3)(res)

    x = Conv2D(n_filters_output, (3, 3), strides=(1, 1), padding="same")(out)
    x = BatchNormalization()(x)
    x = Activation(activation)(x)
    x = Dropout(dropout)(x) if dropout else x

    if downsampling:
        x = UpSampling2DBilinear(img_shape[0:2])(x)

    x = Conv2D(output_ch, (1, 1), strides=(1, 1))(x)
    x = Activation('sigmoid')(x)
    model = Model(inputs=inputs_img, outputs=x)
    return model


def UpSampling2DBilinear(size):
    return Lambda(lambda x: tf.image.resize_bilinear(x, size,
                                                     align_corners=True))


def pyramide(inputs, kernel_size=10, stride=10,
             n_filters=64, output_shape=(728, 728), activation='relu'):
    """
    Pyramid layers:
    img -> downsample (averagepooling) -> conv2d -> upsampling
    """
    pyr = AveragePooling2D(pool_size=(kernel_size, kernel_size),
                           strides=stride, padding='same')(inputs)
    pyr = Conv2D(n_filters, (1, 1), strides=(1, 1))(pyr)
    pyr = BatchNormalization()(pyr)
    pyr = Activation(activation)(pyr)
    pyr = UpSampling2DBilinear((output_shape))(pyr)
    return pyr

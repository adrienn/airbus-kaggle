# -*- coding: utf-8 -*-

from keras.models import Model
from keras.layers import (BatchNormalization,
                          UpSampling2D,
                          concatenate,
                          Input,
                          Conv2D,
                          Dropout,
                          MaxPooling2D)
from keras.utils.data_utils import get_file

WEIGHTS_PATH = ('https://github.com/fchollet/deep-learning-models/'
                'releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels.h5')
WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'

IMAGE_ORDERING = 'channels_last'


def create_model(img_shape=(768, 768, 3), activation='relu',
                 dropout=False):

    img_input = Input(shape=img_shape)

    x = Conv2D(64, (3, 3), activation=activation, padding='same',
               name='block1_conv1', data_format=IMAGE_ORDERING)(img_input)
    x = Conv2D(64, (3, 3), activation=activation, padding='same',
               name='block1_conv2', data_format=IMAGE_ORDERING)(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool',
                     data_format=IMAGE_ORDERING)(x)
    f1 = x
    # Block 2
    x = Conv2D(128, (3, 3), activation=activation, padding='same',
               name='block2_conv1', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(128, (3, 3), activation=activation, padding='same',
               name='block2_conv2', data_format=IMAGE_ORDERING)(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool',
                     data_format=IMAGE_ORDERING)(x)
    f2 = x

    # Block 3
    x = Conv2D(256, (3, 3), activation=activation, padding='same',
               name='block3_conv1', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(256, (3, 3), activation=activation, padding='same',
               name='block3_conv2', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(256, (3, 3), activation=activation, padding='same',
               name='block3_conv3', data_format=IMAGE_ORDERING)(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool',
                     data_format=IMAGE_ORDERING)(x)
    f3 = x

    # Block 4
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block4_conv1', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block4_conv2', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block4_conv3', data_format=IMAGE_ORDERING)(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool',
                     data_format=IMAGE_ORDERING)(x)

    # Block 5
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block5_conv1', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block5_conv2', data_format=IMAGE_ORDERING)(x)
    x = Conv2D(512, (3, 3), activation=activation, padding='same',
               name='block5_conv3', data_format=IMAGE_ORDERING)(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool',
                     data_format=IMAGE_ORDERING)(x)

    vgg = Model(img_input, x)
    weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                            WEIGHTS_PATH_NO_TOP,
                            cache_subdir='models')
    vgg.load_weights(weights_path)

    o = f3

    # o = (Conv2D(512, (3, 3), activation=activation,
    #             padding='same', data_format=IMAGE_ORDERING))(o)
    # o = (BatchNormalization())(o)
    # o = (UpSampling2D((2, 2), data_format=IMAGE_ORDERING))(o)
    # o = (concatenate([o, f4], axis=3))

    # o = (Conv2D(256, (3, 3), activation=activation,
    #             padding='same', data_format=IMAGE_ORDERING))(o)
    # o = (BatchNormalization())(o)
    # o = (UpSampling2D((2, 2), data_format=IMAGE_ORDERING))(o)
    # o = (concatenate([o, f3], axis=3))

    o = (Conv2D(128, (3, 3), activation=activation,
                padding='same', data_format=IMAGE_ORDERING))(o)
    o = (BatchNormalization())(o)
    o = Dropout(dropout)(o) if dropout else o
    o = (UpSampling2D((2, 2), data_format=IMAGE_ORDERING))(o)
    o = (concatenate([o, f2], axis=3))

    o = (Conv2D(64, (3, 3), activation=activation,
                padding='same', data_format=IMAGE_ORDERING))(o)
    o = (BatchNormalization())(o)
    o = Dropout(dropout)(o) if dropout else o
    o = (UpSampling2D((2, 2), data_format=IMAGE_ORDERING))(o)
    o = (concatenate([o, f1], axis=3))

    o = (Conv2D(64, (3, 3), activation=activation,
                padding='same', data_format=IMAGE_ORDERING))(o)
    o = (BatchNormalization())(o)
    o = Dropout(dropout)(o) if dropout else o
    o = (UpSampling2D((2, 2), data_format=IMAGE_ORDERING))(o)

    o = Conv2D(1, (3, 3), padding='same',
               activation='sigmoid',
               data_format=IMAGE_ORDERING)(o)

    return Model(inputs=img_input, outputs=o)

# -*- coding: utf-8 -*-

from keras.models import Input, Model
from keras.layers import (Conv2D, Concatenate, MaxPooling2D,
                          UpSampling2D, Dropout, BatchNormalization)


def conv_block(m, dim, acti,
               bn, res, do=0):
    n = Conv2D(dim, 3, activation=acti, padding='same')(m)
    n = BatchNormalization()(n) if bn else n
    n = Dropout(do)(n) if do else n
    n = Conv2D(dim, 3, activation=acti, padding='same')(n)
    n = BatchNormalization()(n) if bn else n
    return Concatenate()([m, n]) if res else n


def level_block(m, dim, depth, inc,
                acti, do, bn, mp, up, res):
    if depth > 0:
        n = conv_block(m, dim, acti, bn, res)
        m = (MaxPooling2D()(n) if mp
             else Conv2D(dim, 3, strides=2, padding='same')(n))
        m = level_block(m, int(inc*dim), depth-1, inc,
                        acti, do, bn, mp, up, res)

        if up:
            m = UpSampling2D()(m)
            m = Conv2D(dim, 2, activation=acti, padding='same')(m)
    else:
        m = conv_block(m, dim, acti, bn, res, do)
    return m


def create_model(img_shape=(768, 768, 3), out_ch=1, start_ch=4, depth=4,
                 inc_rate=2., activation='relu', dropout=.5,
                 batch_norm=False, max_pool=False, up_conv=True,
                 residual=False):
    """
    Unet model: Convolutional Networks for Biomedical Image Segmentation
                (https://arxiv.org/abs/1505.04597)

    Parameters:

    img_shape: (height, width, channels)
    out_ch: number of output channels 
    start_ch: number of channels of the first conv
    depth: zero indexed depth of the U-structure
    inc_rate: rate at which the conv channels will increase
    activation: activation function for each conv
    dropout: amount of dropout
    batch_norm: boolean for batch normalization
    max_pool: True for max_pooling, False for strided conv
    upconv: True for upsampling+conv False for transposed conv
    residual: boolean for adding residual connections around each conv block
    """

    i = Input(shape=img_shape)
    o = level_block(i, start_ch, depth, inc_rate, activation,
                    dropout, batch_norm, max_pool, up_conv,
                    residual)
    o = Conv2D(out_ch, 1, activation='sigmoid')(o)

    return Model(input=i, outputs=o)
